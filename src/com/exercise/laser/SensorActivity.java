package com.exercise.laser;

import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;

import java.net.MalformedURLException;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ToggleButton;

import com.exercise.laser.SensorFusion.Rotation;
import com.exercise.pointer.R;

public class SensorActivity extends Activity  implements Observer {

	private static final String SERVER = "http://laser-pointer.fampinheiro.com";
	
	private SocketIO socket;
	private SensorFusion sensor;
	private Button resetButton;
	private ToggleButton statusButton;

	private void connect() {
		if (socket != null && socket.isConnected())
			return;
		
		try {
			socket = new SocketIO(SERVER);
			socket.connect(new IOCallback() {

				@Override
				public void onMessage(JSONObject json, IOAcknowledge ack) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onMessage(String data, IOAcknowledge ack) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onError(SocketIOException socketIOException) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onDisconnect() {
					Log.i(CONNECTIVITY_SERVICE, "Disconnected");

				}

				@Override
				public void onConnect() {
					Log.i(CONNECTIVITY_SERVICE, "Connected");

				}

				@Override
				public void on(String event, IOAcknowledge ack, Object... args) {
					// TODO Auto-generated method stub

				}
			});
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sensor);

		statusButton = (ToggleButton)findViewById(R.id.toggle_button);
		statusButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (statusButton.isChecked()) {
					connect();
				} else {
					if (socket != null && socket.isConnected())
						socket.disconnect();
				}
								
			}
		});
		
		resetButton = (Button)findViewById(R.id.button_reset);
		resetButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				sensor.reset();
			}
		});
				
		sensor = new SensorFusion();
		sensor.init((SensorManager) getSystemService(SENSOR_SERVICE));
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		sensor.addObserver(this);
		sensor.resume();
		
		if (statusButton.isPressed())
			connect();
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (socket != null && socket.isConnected())
			socket.disconnect();

		sensor.pause();
		sensor.deleteObserver(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		
		if (socket != null && socket.isConnected())
			socket.disconnect();

		sensor.pause();
		sensor.deleteObserver(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_rotation, menu);
		return true;
	}

	@Override
	public void update(Observable observable, Object data) {
		if (!(data instanceof Rotation))
			return;
		
		Rotation rotation = (Rotation) data;
		if (socket != null && socket.isConnected()) {
			try {
				JSONObject json = new JSONObject();

				json.put("x", rotation.azimuth);
				json.put("y", rotation.pitch);

				socket.send(json);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
