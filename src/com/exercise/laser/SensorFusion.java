package com.exercise.laser;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import com.exercise.laser.util.Matrix;


import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class SensorFusion extends Observable implements SensorEventListener {

	private class Filter {
		static final int FILTER_SIZE = 50;
		float buffer[] = new float[FILTER_SIZE];
		int currentIndex = 0;

		public float append(float value) {
			buffer[currentIndex] = value;
			currentIndex++;
			if (currentIndex == FILTER_SIZE)
				currentIndex = 0;
			return average();
		}

		public float average() {
			float sum = 0;
			for (float val : buffer)
				sum += val;
			return sum / FILTER_SIZE;
		}
	}

	class Calculation extends TimerTask {

		@Override
		public void run() {
			if (filteredOrientation == null) {
				return;
			}

			float oneMinusCoeff = 1.0f - FILTER_COEFFICIENT;
			fusedOrientation[0] = FILTER_COEFFICIENT * gyroscopeOrientation[0]
					+ oneMinusCoeff * filteredOrientation[0];
			fusedOrientation[1] = FILTER_COEFFICIENT * gyroscopeOrientation[1]
					+ oneMinusCoeff * filteredOrientation[1];
			fusedOrientation[2] = FILTER_COEFFICIENT * gyroscopeOrientation[2]
					+ oneMinusCoeff * filteredOrientation[2];

			gyroscopeM = getRotationMatrixFromOrientation(fusedOrientation);
			System.arraycopy(fusedOrientation, 0, gyroscopeOrientation, 0, 3);

			SensorFusion.this.setChanged();
			SensorFusion.this.notifyObservers(new Rotation(fusedOrientation[0]
					- orientationOffset[0], fusedOrientation[1]
					- orientationOffset[1], fusedOrientation[2]
					- orientationOffset[2]));

		}

	}

	public static final String TAG = "Fusion";
	public static final float EPSILON = 0.000000001f;
	public static final int TIME_CONSTANT = 60;
	public static final float FILTER_COEFFICIENT = 0.98f;

	// general sensor variables
	private float[] gyroscope = new float[3];
	private float[] accel = new float[3];
	private float[] magnet = new float[3];

	private float[] orientation = new float[3];
	private float[] filteredOrientation = null;
	private float[] gyroscopeOrientation = new float[3];
	private float[] fusedOrientation = new float[3];
	private float[] orientationOffset = new float[3];
	private float[] gyroscopeM = new float[9];

	private boolean calibrated = true;
	private Filter filterArray[] = { new Filter(), new Filter(), new Filter() };

	// gyroscope stuff
	private static final float NS2S = 1.0f / 1000000000.0f;
	private float[] rotationM = new float[16];
	private float timestamp;

	private Timer fuseTimer = new Timer();
	
	private SensorManager manager;
	private Sensor acce;
	private Sensor magn;
	private Sensor gyro;

	public SensorFusion() {
		gyroscopeOrientation[0] = 0.0f;
		gyroscopeOrientation[1] = 0.0f;
		gyroscopeOrientation[2] = 0.0f;

		orientationOffset[0] = 0.0f;
		orientationOffset[1] = 0.0f;
		orientationOffset[2] = 0.0f;

		gyroscopeM[0] = 1.0f;
		gyroscopeM[1] = 0.0f;
		gyroscopeM[2] = 0.0f;
		gyroscopeM[3] = 0.0f;
		gyroscopeM[4] = 1.0f;
		gyroscopeM[5] = 0.0f;
		gyroscopeM[6] = 0.0f;
		gyroscopeM[7] = 0.0f;
		gyroscopeM[8] = 1.0f;

		fuseTimer.scheduleAtFixedRate(new Calculation(), 1000, TIME_CONSTANT);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		Log.i("SENSOR_FUGION", "[" + sensor.getType()
				+ "] Accuracy changed to " + accuracy + ".");
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE)
			return;

		switch (event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			processAccelerometer(event);
			break;
		case Sensor.TYPE_MAGNETIC_FIELD:
			processMagnet(event);
			break;
		case Sensor.TYPE_GYROSCOPE:
			processGyroscope(event);
			break;

		}
	}

	public void init(SensorManager sensorManager) {
		manager = sensorManager;
		acce = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		magn = manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		gyro = manager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
	}

	public void resume() {

		manager.registerListener(this, acce,
				SensorManager.SENSOR_DELAY_FASTEST);
		manager.registerListener(this, magn,
				SensorManager.SENSOR_DELAY_FASTEST);
		manager.registerListener(this, gyro, SensorManager.SENSOR_DELAY_FASTEST);
	}

	public void pause() {
		manager.unregisterListener(this);
	}

	public void reset() {
		orientationOffset[0] = fusedOrientation[0];
		orientationOffset[1] = fusedOrientation[1];
		orientationOffset[2] = fusedOrientation[2];
	}

	private void processAccelerometer(SensorEvent event) {
		System.arraycopy(event.values, 0, accel, 0, 3);
		calculateOrientation();
	}

	private void processMagnet(SensorEvent event) {
		System.arraycopy(event.values, 0, magnet, 0, 3);
	}

	private void processGyroscope(SensorEvent event) {
		if (orientation == null)
			return;

		if (calibrated) {
			float[] matrix = new float[9];
			matrix = getRotationMatrixFromOrientation(orientation);
			float[] dummy = new float[3];
			SensorManager.getOrientation(matrix, dummy);
			gyroscopeM = Matrix.multiply(gyroscopeM, matrix);
			calibrated = false;
		}

		float[] deltaVector = new float[4];
		if (timestamp != 0) {
			final float dT = (event.timestamp - timestamp) * NS2S;
			System.arraycopy(event.values, 0, gyroscope, 0, 3);
			getRotationVectorFromGyroscope(gyroscope, deltaVector, dT / 2.0f);
		}
		timestamp = event.timestamp;

		float[] deltaMatrix = new float[9];
		SensorManager.getRotationMatrixFromVector(deltaMatrix, deltaVector);

		gyroscopeM = Matrix.multiply(gyroscopeM, deltaMatrix);
		SensorManager.getOrientation(gyroscopeM, gyroscopeOrientation);
	}

	private void calculateOrientation() {
		if (SensorManager.getRotationMatrix(rotationM, null, accel, magnet)) {
			SensorManager.getOrientation(rotationM, orientation);

			if (filteredOrientation == null) {
				filteredOrientation = new float[9];
			}

			filteredOrientation[0] = filterArray[0].append(orientation[0]);
			filteredOrientation[1] = filterArray[1].append(orientation[1]);
			filteredOrientation[2] = filterArray[2].append(orientation[2]);
		}
	}

	private void getRotationVectorFromGyroscope(float[] gyroscopeAngles,
			float[] resultQuaternion, float factor) {
		float[] unitEuler = new float[3];
		float omegaMagnitude = (float) Math.sqrt(gyroscopeAngles[0]
				* gyroscopeAngles[0] + gyroscopeAngles[1] * gyroscopeAngles[1]
				+ gyroscopeAngles[2] * gyroscopeAngles[2]);
		if (omegaMagnitude > EPSILON) {
			unitEuler[0] = gyroscopeAngles[0] / omegaMagnitude;
			unitEuler[1] = gyroscopeAngles[1] / omegaMagnitude;
			unitEuler[2] = gyroscopeAngles[2] / omegaMagnitude;
		}

		float thetaOverTwo = omegaMagnitude * factor;
		float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
		float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
		resultQuaternion[0] = sinThetaOverTwo * unitEuler[0];
		resultQuaternion[1] = sinThetaOverTwo * unitEuler[1];
		resultQuaternion[2] = sinThetaOverTwo * unitEuler[2];
		resultQuaternion[3] = cosThetaOverTwo;
	}

	private float[] getRotationMatrixFromOrientation(float[] o) {
		float[] xM = new float[9];
		float[] yM = new float[9];
		float[] zM = new float[9];

		float sinX = (float) Math.sin(o[1]);
		float cosX = (float) Math.cos(o[1]);
		float sinY = (float) Math.sin(o[2]);
		float cosY = (float) Math.cos(o[2]);
		float sinZ = (float) Math.sin(o[0]);
		float cosZ = (float) Math.cos(o[0]);

		xM[0] = 1.0f; xM[1] = 0.0f; xM[2] = 0.0f;
		xM[3] = 0.0f; xM[4] = cosX; xM[5] = sinX;
		xM[6] = 0.0f; xM[7] = -sinX; xM[8] = cosX;

		yM[0] = cosY; yM[1] = 0.0f; yM[2] = sinY;
		yM[3] = 0.0f; yM[4] = 1.0f; yM[5] = 0.0f;
		yM[6] = -sinY; yM[7] = 0.0f; yM[8] = cosY;

		zM[0] = cosZ; zM[1] = sinZ; zM[2] = 0.0f;
		zM[3] = -sinZ; zM[4] = cosZ; zM[5] = 0.0f;
		zM[6] = 0.0f; zM[7] = 0.0f; zM[8] = 1.0f;
		
		float[] result = Matrix.multiply(xM, yM);
		result = Matrix.multiply(zM, result);
		return result;
	}

	public static class Rotation {
		public final float azimuth;
		public final float pitch;
		public final float roll;

		public Rotation(float azimuth, float pitch, float roll) {
			this.azimuth = azimuth;
			this.pitch = pitch;
			this.roll = roll;
		}

		public Rotation(float[] values) {
			this(values[0], values[1], values[2]);
		}

	}

}
