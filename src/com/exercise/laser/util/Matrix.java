package com.exercise.laser.util;

public final class Matrix {

	public static float[] multiply(float[] a, float[] b) {
		if (a.length != b.length)
			return null;

		int size = a.length;
		double sqrt = Math.sqrt(size);
		if (sqrt % 1 != 0)
			return null;

		int side = (int) sqrt;
		float[] result = new float[a.length];
		for (int i = 0; i < side; i++) {
			for (int j = 0; j < side; j++) {
				result[i + j * side] = 0;
				for (int k = 0; k < side; k++) {
					result[i + j * side] += a[i + k * side] * b[k + j * side];
				}

			}

		}
		return result;
	}
}
