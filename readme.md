# Laser pointer Android

Laser pointer Android is a client of the [Laser pointer](http//github.com/fampinheiro/laser-pointer) project.

## Run locally

To run locally just clone the repository and compile the code.

``` bash 
git clone git://github.com/fampinheiro/laser-pointer-android.git
cd laser-pointer-android
``` 
Before running the app make sure to change SensorActivity.SERVER variable to your server url.
